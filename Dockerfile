FROM python:3.8

## Secrets
ARG AWS_ACCESS_KEY_ID
ARG AWS_SECRET_ACCESS_KEY

# Parameters for the iptell job
ARG DOMAIN_NAME
ARG TTL
ARG HOSTED_ZONE_ID

# Some of these arguments need to be made into environment
# variables for the cron job
ENV DOMAIN_NAME=$DOMAIN_NAME \
    TTL=$TTL \
    HOSTED_ZONE_ID=$HOSTED_ZONE_ID

# Install pipenv
RUN pip install pipenv

# Copy in our python application files
RUN mkdir /app
COPY iptell.py /app/iptell.py
COPY Pipfile /app/Pipfile
RUN chmod +x /app/iptell.py

# Install the python application dependencies
RUN cd /app && pipenv lock && pipenv sync && pipenv run pip freeze > requirements.txt && pip install -r requirements.txt

# Create a boto credentials file
RUN mkdir -p /root/.aws && \
    echo "[default]" > /root/.aws/credentials && \
    echo "aws_access_key_id=${AWS_ACCESS_KEY_ID}" >> /root/.aws/credentials && \
    echo "aws_secret_access_key=${AWS_SECRET_ACCESS_KEY}" >> /root/.aws/credentials

# Install cron
RUN apt update && apt install -y cron

# Create a crontab file
RUN echo "DOMAIN_NAME=$DOMAIN_NAME" > /etc/cron.d/iptell-env && \
    echo "TTL=$TTL" >> /etc/cron.d/iptell-env && \
    echo "HOSTED_ZONE_ID=$HOSTED_ZONE_ID" >> /etc/cron.d/iptell-env

# Copy in the crontab file
COPY iptell-cron /etc/cron.d/iptell-cron.tmp
RUN cat /etc/cron.d/iptell-env /etc/cron.d/iptell-cron.tmp > /etc/cron.d/iptell-cron
RUN chmod 0644 /etc/cron.d/iptell-cron

# Apply cron job
RUN crontab /etc/cron.d/iptell-cron

# Run the command on container startup
CMD cron -f

