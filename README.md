iptell
=====

A simple dynamic DNS solution which queries ipify for your public IP address, then updates a route53 hosted zone to map a domain name to that address.

# How Does It Work?

The script `iptell.py` is invoked like so:

```
iptell.py --domain_name home.mydomain.com --ttl 300 --use_ipv6 --hosted_zone_id AZ1234567
```

**Parameters:**

* `--domain_name` - The domain name you want to update. This domain _must_ be part of the hosted zone identified by `--hosted_zone_id`.
* `--ttl` - The TTL to set on the domain.
* `--use_ipv6` - This is optional. If you have an IPv6 address, then an `AAAA` record will be created in addition to an `A` record. If you don't have an IPv6 address, then this does nothing.
* `--hosted_zone_id` - The route53 hosted zone ID. You can find this in the AWS Management Console.

This command is idempotent. It will either create the record(s) if they don't exist, update the record(s) if needed, or do nothing if the record(s) are already up-to-date.

> Careful: the script performs a DNS lookup to determine if the record(s) are up-to-date. If your hosted zone was recently updated, the script may not detect that until the TTL has expired. It is recommended to run this script in an interval longer than your TTL to avoid unnecessary route53 updates.

The provided `Dockerfile` will build an image that runs this script at an interval using `cron`. The interval _should_ (see caveat at the end) depend on the specified TTL for your domain.

# Running the Service

First create a `.env` file and place it in the same directory as the `docker-compose.yml` file. This file will provide environment variables specific to your setup to docker-compose. The file should look like this:

```bash
DOMAIN_NAME=<your domain name>
TTL=<TTL>
HOSTED_ZONE_ID=<route53 hosted zone ID>
AWS_ACCESS_KEY_ID=<AWS Access Key Id>
AWS_SECRET_ACCESS_KEY=<AWS Secret Access Key>
```

The `HOSTED_ZONE_ID`, `AWS_ACCESS_KEY_ID`, and `AWS_SECRET_ACCESS_KEY` can be found in your Amazon AWS Management Console.

**IAM Permissions**

You'll need to create an IAM user with the following permissions:

* Write - `ChangeResourceRecordSets`. It's recommended to restrict this to only the relevant Hosted Zone.

Once you've created the user, you should save the AWS Access Key Id and the AWS Secret Access Key. You won't be able to retrieve these later.

**Running Docker**

Once the `.env` file is in place, you should simply be able to run:

```
docker-compose up
```

If you need to update the `.env` file, be sure to rebuild the image:

```
docker-compose build
```

or

```
docker-compose up --build
```

Once you're ready to deploy this into production, you'll probably want to run the service as a daemon:

```
docker-compose up -d
```

Then you can stop it with:

```
docker-compose down
```

# What Needs Work Still?

* Honestly, I haven't fully tested IPv6 functionality because I'm not sure that I have an IPv6 address. When I query ipify's ipv6 API, it returns my IPv4 address, so I had simply assumed I don't have an IPv6. This isn't high on my priority list so I'm not likely to investigate much further until I need to.
* Some sort of error reporting mechanism would be nice. If this service isn't working, how will I know? Can it be instrumented so that I'll get a notification from this service or from some monitor on the system that's running it?
* The script could query route53 to be smarter about when to update the DNS entry. For now I'm happy with the solution to just run the script at intervals longer than the TTL.
* Okay, I'm a terrible person. The cron interval is hardcoded to every 5 minutes. It doesn't depend on the TTL at all. This works for me because my TTL is 300 seconds. If you're a person who is not me and you want to use this service, then beware. I will happily accept patches, or we can work together to figure out a better way.

