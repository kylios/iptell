#!/usr/bin/env python3

import argparse
import boto3
import requests
import socket

'''
A simple dynamic DNS service that runs locally. This script looks up the current public-facing
IP address by querying ipify, and then updates a given domain name in route53 to point to the
IP. This script should be run periodically depending on how frequently (and when) your ISP
might update your IP address.
'''


IPIFY_V4_URL = 'https://api.ipify.org?format=json'
IPIFY_V6_URL = 'https://api64.ipify.org?format=json'


class NoIpException(Exception):
    def __init__(self, url, status_code, body):
        super(NoIpException, self).__init__('''
No IP address in response to {}.

Status Code: {}:

{}
'''.format(url, status_code, body))


def parse_args():
    parser = argparse.ArgumentParser(
        description='Update a DNS entry hosted in route53 to point to our IP address'
    )
    parser.add_argument(
        '--domain_name', required=True,
        type=str, help='The domain name to map to this IP address.'
    )
    parser.add_argument(
        '--ttl', default=300,
        type=int, help='The TTL to set on the domain name.'
    )
    parser.add_argument(
        '--use_ipv6', default=True,
        type=bool, help='Attempt to add an AAAA record for ipv6 addresses (fails gracefully if ipv6 not supported).'
    )
    parser.add_argument(
        '--hosted_zone_id', required=True,
        type=str, help='The AWS ID of the hosted zone.'
    )

    return parser.parse_args()


def get_ipv4():
    resp = requests.get(IPIFY_V4_URL)
    ipv4 = resp.json().get('ip')
    if not ipv4:
        raise NoIpException(IPIFY_V4_URL, resp.status_code, resp.text)
    return ipv4


def get_ipv6():
    resp = requests.get(IPIFY_V6_URL)
    ipv6 = resp.json().get('ip')
    if not ipv6:
        raise NoIpException(IPIFY_V6_URL, resp.status_code, resp.text)
    return ipv6


def record_set(domain_name, record_type, ttl, ip):
    return {
        'Name': domain_name,
        'Type': record_type,
        'TTL': ttl,
        'ResourceRecords': [
            {
                'Value': ip
            }
        ]
    }


def update_dns_record(record_sets, hosted_zone_id):
    print('Updating DNS')

    route53 = boto3.client('route53')

    # https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/route53.html#Route53.Client.change_resource_record_sets
    response = route53.change_resource_record_sets(
        HostedZoneId=hosted_zone_id,
        ChangeBatch={
            'Comment': 'Automatic update from iptell',
            'Changes': [
                {
                    'Action': 'UPSERT',
                    'ResourceRecordSet': record_set
                }
                for record_set in record_sets
            ]
        }
    )


def dns_lookup(domain_name):
    try:
        return socket.getaddrinfo(domain_name, None)
    except socket.gaierror:
        # Raised if the domain name doesn't exist
        return None


def ip_in_addr_info(addr_info, ip):
    if not addr_info:
        return False

    # See docs on `socket.getaddrinfo`
    for addr in addr_info:
        if addr[4][0] == ip:
            return True
    return False


if __name__ == '__main__':
    args = parse_args()

    domain_name = args.domain_name
    ttl = args.ttl
    use_ipv6 = args.use_ipv6
    hosted_zone_id = args.hosted_zone_id

    # Fetch the existing IP address(es) mapped to this domain. We will only update
    # the IP addresses if they change.
    addr_info = dns_lookup(domain_name)

    # List of record sets to send to route53. We can make several updates in a single
    # atomic API call.
    record_sets = list()
 
    ipv4 = get_ipv4()
    print('IPv4 address is: {}'.format(ipv4))
    if not ip_in_addr_info(addr_info, ipv4):
        record_sets.append(record_set(domain_name, 'A', ttl, ipv4))
    else:
        print('IPv4 address is already set.')

    if use_ipv6:
        try:
            ipv6 = get_ipv6()
            if ipv6 == ipv4:
                print('IPv6 and IPv4 are the same. Do you have an IPv6 address?')
                use_ipv6 = False
            else:
                print('IPv6 address is: {}'.format(ipv6))
                if not ip_in_addr_info(addr_info, ipv6):
                    record_sets.append(record_set(domain_name, 'AAAA', ttl, ipv6))
                else:
                    print('IPv6 address is already set.')
        except Exception as e:
            # Be resilient to failures in IPv6 updates.
            print(e.message)

    if len(record_sets):
        update_dns_record(record_sets, hosted_zone_id)

